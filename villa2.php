<?php include 'top-header.php';?>  
<?php include 'header.php';?>
 <?php
   include("connection.php");
	$conn= new DB_con();
     if(isset($_POST['reviewSubmit'])){
		
		 $month=$_POST['monthReview'];
		$year=$_POST['yearReview'];
		$title=$_POST['titleReview'];
		$your_review=$_POST['messageReview'];
		$address=$_POST['fromReview'];
		$name=$_POST['nameReview'];
		$email=$_POST['emailReview'];
		$res=$conn->insert($month,$year,$title,$your_review,$address,$name,$email);
			if($res){
				?><script>alert("insert succesfully")</script><?php
				header("Location:villa2.php");
			} 
	}
     
     ?>   

	<div class="container">
    	<div id="property_view">  
       
        <div class="search_property col-lg-12">
       		<div class="row">
            	<div class="col-lg-9 col-md-9  col-sm-9  col-xs-12 ">
                <div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs row property_details_tam" role="tablist">
    <li role="presentation"><a href="#Photos" aria-controls="Photos" role="tab" data-toggle="tab">Photos</a></li>
    <li role="presentation"><a href="#Description" aria-controls="Description" role="tab" data-toggle="tab">Description</a></li>
    <li role="presentation"><a href="#Calendar" aria-controls="Calendar" role="tab" data-toggle="tab">Calendar</a></li>
    <li role="presentation"><a href="#Map" aria-controls="Map" role="tab" data-toggle="tab">Map &amp; Area</a></li>
    <li role="presentation"><a href="#Facilities" aria-controls="Facilities" role="tab" data-toggle="tab">Facilities</a></li>
    <li role="presentation"><a href="#Rates" aria-controls="Rates" role="tab" data-toggle="tab">Rates</a></li>
    <li role="presentation"><a href="#Reviews" aria-controls="Reviews" role="tab" data-toggle="tab">Reviews</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="Photos">
    	<div class="row">
        <div  class="white_bg white_bg1 white_bg2">
        		<img src="images/villabanner.jpg" alt=""  width="100%" height="auto">
            </div>
             </div>
        
    </div>
    <div role="tabpanel" class="tab-pane active" id="Description">    	
    	<div class="row">
        	<div class="white_bg white_bg1 white_bg2">
       			<h2>Description</h2>
                <p></p><p>Beautiful 1<strong>2 floor penthouse</strong> located 100% downtown, walking distance to restaurants, beaches, shows, bars, supermarkets and others. Roofed parking space. Only bring your clothes and enjoy this amazing condo. This property has 2 bedrooms, 2 bathrooms, fully furnished and equipped. We have towels, sheets, pillows, blankets and all the kitchen utensils. Both bedrooms are air conditioned and one of the most attractive features is that we have a fine pool table for you located at the center of the living room including a 45 bottles wine cooler.</p>
<p><strong>Payment accept by paypal.com</strong></p><p></p>
<h3>Suitability</h3>
<ul class="white_bg2_ul">
	<li>Children Welcome</li>        <li>Wheelchair Suitable</li>                <li>Smoking Allowed</li>   
</ul><div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane active" id="Calendar">
		<div id="calender_content_view">
    	<div class="row">
        	<div class="white_bg white_bg1 white_bg2">
       			<h2>Calendar</h2>
       			<div id="date3"><iframe src="calendar/index.php?id_item=3597" scrolling="no" style="width:625px;height:550px;border:0px;margin-left:5px;"></iframe></div>
            </div>
        </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane active" id="Map">
    	<div class="row" id="map_content_view">
        	<div class="white_bg white_bg1 white_bg2">
       			<h2>Map</h2>
       			<div><section id="map_canvas" style="position: relative; overflow: hidden; transform: translateZ(0px); background-color: rgb(229, 227, 223);"><div class="gm-style" style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;) 8 8, default;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);"><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 179px; top: 54px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 435px; top: 54px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 435px; top: -202px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 179px; top: -202px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 179px; top: 310px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 435px; top: 310px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: -77px; top: 54px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 691px; top: 54px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: -77px; top: 310px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 691px; top: -202px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: -77px; top: -202px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 691px; top: 310px;"></div></div></div></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 179px; top: 54px;"><canvas draggable="false" height="256" width="256" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 435px; top: 54px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 435px; top: -202px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 179px; top: -202px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 179px; top: 310px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 435px; top: 310px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: -77px; top: 54px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 691px; top: 54px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: -77px; top: 310px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 691px; top: -202px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: -77px; top: -202px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 691px; top: 310px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="transform: translateZ(0px); position: absolute; left: 435px; top: -202px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i10371!3i14673!4i256!2m3!1e0!2sm!3i348015158!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=104426" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 179px; top: -202px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i10370!3i14673!4i256!2m3!1e0!2sm!3i348015002!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=9464" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 179px; top: 54px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i10370!3i14674!4i256!2m3!1e0!2sm!3i348015002!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=45485" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 691px; top: -202px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i10372!3i14673!4i256!2m3!1e0!2sm!3i348015158!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=64705" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 691px; top: 54px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i10372!3i14674!4i256!2m3!1e0!2sm!3i348015158!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=100726" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 435px; top: 54px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i10371!3i14674!4i256!2m3!1e0!2sm!3i348015158!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=9376" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: -77px; top: -202px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i10369!3i14673!4i256!2m3!1e0!2sm!3i348015002!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=41593" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: -77px; top: 54px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i10369!3i14674!4i256!2m3!1e0!2sm!3i348015002!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=77614" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 435px; top: 310px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i10371!3i14675!4i256!2m3!1e0!2sm!3i348015974!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=91203" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: -77px; top: 310px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i10369!3i14675!4i256!2m3!1e0!2sm!3i348015974!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=31982" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 179px; top: 310px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i10370!3i14675!4i256!2m3!1e0!2sm!3i348015974!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=130924" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 691px; top: 310px; transition: opacity 200ms ease-out;"><img src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i10372!3i14675!4i256!2m3!1e0!2sm!3i348015974!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=51482" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div class="gm-style-pbc" style="position: absolute; left: 0px; top: 0px; z-index: 2; width: 100%; height: 100%; transition-duration: 0.3s; opacity: 0; display: none;"><p class="gm-style-pbt">Use two fingers to move the map</p></div><div style="position: absolute; left: 0px; top: 0px; z-index: 3; width: 100%; height: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 4; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);"><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a target="_blank" href="https://maps.google.com/maps?ll=18.450829,-66.062955&amp;z=15&amp;t=m&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3" title="Click to see this area on Google Maps" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/google4.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></a></div><div style="padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 235px; top: 159px; background-color: white;"><div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div><div style="font-size: 13px;">Map data ©2016 Google</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/mapcnt6.png" draggable="false" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div><div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 256px; bottom: 0px; width: 122px;"><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Map Data</a><span style="">Map data ©2016 Google</span></div></div></div><div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;"><div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Map data ©2016 Google</div></div><div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; -webkit-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 97px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/intl/en-US_US/help/terms_maps.html" target="_blank" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Terms of Use</a></div></div><div style="width: 25px; height: 25px; overflow: hidden; display: none; margin: 10px 14px; position: absolute; top: 0px; right: 0px;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/sv5.png" draggable="false" class="gm-fullscreen-control" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 112px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_new" title="Report errors in the road map or imagery to Google" href="https://www.google.com/maps/@18.450829,-66.062955,15z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Report a map error</a></div></div><div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" draggable="false" controlwidth="28" controlheight="93" style="margin: 10px; -webkit-user-select: none; position: absolute; bottom: 107px; right: 28px;"><div class="gmnoprint" controlwidth="28" controlheight="55" style="position: absolute; left: 0px; top: 38px;"><div draggable="false" style="-webkit-user-select: none; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-radius: 2px; cursor: pointer; width: 28px; height: 55px; background-color: rgb(255, 255, 255);"><div title="Zoom in" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: 0px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div><div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; top: 0px; background-color: rgb(230, 230, 230);"></div><div title="Zoom out" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: -15px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div></div></div><div class="gm-svpc" controlwidth="28" controlheight="28" style="box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-radius: 2px; width: 28px; height: 28px; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;) 8 8, default; position: absolute; left: 0px; top: 0px; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: 1px;"></div><div style="position: absolute; left: 1px; top: 1px;"><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -26px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Pegman is on top of the Map" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -52px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="https://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -78px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div class="gmnoprint" controlwidth="28" controlheight="0" style="display: none; position: absolute;"><div title="Rotate map 90 degrees" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; cursor: pointer; display: none; background-color: rgb(255, 255, 255);"><img src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div class="gm-tilt" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; top: 0px; cursor: pointer; background-color: rgb(255, 255, 255);"><img src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div class="gmnoprint" style="margin: 10px; z-index: 0; position: absolute; cursor: pointer; left: 0px; top: 0px;"><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Show street map" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 8px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; -webkit-background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; min-width: 22px; font-weight: 500; background-color: rgb(255, 255, 255); background-clip: padding-box;">Map</div><div style="z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; left: 0px; top: 31px; text-align: left; display: none; background-color: white;"><div draggable="false" title="Show street map with terrain" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap; background-color: rgb(255, 255, 255);"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img src="https://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Terrain</label></div></div></div><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Show satellite imagery" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 8px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; -webkit-background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-left-width: 0px; min-width: 41px; background-color: rgb(255, 255, 255); background-clip: padding-box;">Satellite</div><div style="z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; right: 0px; top: 31px; text-align: left; display: none; background-color: white;"><div draggable="false" title="Show imagery with street names" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap; background-color: rgb(255, 255, 255);"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden;"><img src="https://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Labels</label></div></div></div></div><div draggable="false" class="gm-style-cc" style="position: absolute; -webkit-user-select: none; height: 14px; line-height: 14px; right: 169px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><span>200 m&nbsp;</span><div style="position: relative; display: inline-block; height: 8px; bottom: -1px; width: 44px;"><div style="width: 100%; height: 4px; position: absolute; left: 0px; top: 0px;"></div><div style="width: 4px; height: 8px; left: 0px; top: 0px; background-color: rgb(255, 255, 255);"></div><div style="width: 4px; height: 8px; position: absolute; left: 0px; bottom: 0px; background-color: rgb(255, 255, 255);"></div><div style="position: absolute; height: 2px; left: 1px; bottom: 1px; right: 1px; background-color: rgb(102, 102, 102);"></div><div style="position: absolute; width: 2px; height: 6px; left: 1px; top: 1px; background-color: rgb(102, 102, 102);"></div><div style="width: 2px; height: 6px; position: absolute; bottom: 1px; right: 1px; background-color: rgb(102, 102, 102);"></div></div></div></div></div></section></div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
     <div role="tabpanel" class="tab-pane active" id="Facilities">
     	<div class="row">
        	<div class="white_bg white_bg1 white_bg2">
       			<h2>Facilities</h2>
                <h3>Bedroom </h3>
                <div class="table-responsive">
             <table class="table table-bordered">
                    <thead>
                      <tr>
                      	<th width="50px">Bedroom Name</th>
                        <th width="60px">Beds King</th>
                        <th width="50px">Type</th>
                        <th width="50px">Double</th>
                        <th width="50px">Queen</th>
                        <th width="50px">Child/Bunk</th>
                        <th width="50px">Cot</th>
                        <th width="50px">Single</th>
                        <th width="50px">Sofabed/futon</th>
                        <th width="50px">Notes</th>
                        
                        </tr>
                    </thead>
                    <tbody>
                                        <tr>
                        <td>Master Bedroom Shower &amp; Jacuzz</td>
                        <td>1</td>
                        <td>Master Bedroom</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td></td>
                    </tr>
						                    <tr>
                        <td>Bedroom 2</td>
                        <td>0</td>
                        <td>Queen Bedroom</td>
                        <td>0</td>
                        <td>1</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td></td>
                    </tr>
						                    </tbody>
                  </table>
                  </div>
                   <h3>Bathroom  </h3>
                <div class="table-responsive">
             <table class="table table-bordered">
                    <thead>
                      <tr>
                      	<th width="50px">Bathroom Name</th>
                        <th width="50px">Beds King.</th>
                        <th width="50px">Type</th>
                        <th width="50px">Bath</th>
                        <th width="50px">Shower</th>
                        <th width="50px">Cabinet</th>
                        <th width="50px">Spa bath</th>
                        <th width="50px">Tub</th>
                        <th width="50px">Bidet</th>
                        <th width="50px">Notes</th>
                        
                        </tr>
                    </thead>
                    <tbody>
                                        <tr>
                        <td>Master bathroom</td>
                        <td>0</td>
                        <td>Full Bathroom (separate)</td>
                        <td>1</td>
                        <td>1</td>
                        <td>0</td>
                        <td>0</td>
                        <td>1</td>
                        <td>0</td>
                        <td></td>
                  </tr>
                                        <tr>
                        <td>Bathroom 2</td>
                        <td>0</td>
                        <td>Full Bathroom (separate)</td>
                        <td>1</td>
                        <td>1</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td></td>
                  </tr>
                      
                    </tbody>
                  </table>
                  </div>
                  <h2>Amenities</h2>
            
                  
                   <h2 style="padding:0px;">Other room</h2>
                        <ul class="tab_c">

                        	<li>Kitchen</li><li>Family room</li><li>Living room
                       </li></ul>

                       <div style="clear:both;"></div>

                  
                  
                  
                  
                  
                  <h3>Features / Facilities</h3>
                 <ul class="white_bg2_ul">
                    									<li>Guests Provide Their Own Meals</li>
																	<li>Children Welcome</li>
																	<li>Pets Not Allowed</li>
																	<li>Satellite / Cable</li>
																	<li>Television: 3 LED; 40", 52" and 60"</li>
																	<li>Pool Table</li>
																	<li>Stereo</li>
																	<li>Dining Area Seating for 4 people</li>
																	<li>Air Conditioning:</li>
																	<li>Clothes Dryer</li>
																	<li>Internet</li>
																	<li>Parking</li>
																	<li>Hair Dryer</li>
																	<li>Linens Provided</li>
																	<li>Washing Machine</li>
																	<li>Coffee Maker</li>
																	<li>Oven</li>
																	<li>Stove</li>
																	<li>Pantry Items</li>
																	<li>Toaster</li>
																	<li>Refrigerator</li>
																	<li>Balcony</li>
																	<li>Boat</li>
																	<li>Kayak / Canoe</li>
																	<li>Bicycles</li>
																	<li>Water Sports Gear</li>
																	<li>Golf: 2 miles</li>
								                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
     </div>
      <div role="tabpanel" class="tab-pane active" id="Rates">
      	<div class="row">
        	<div class="white_bg white_bg1 white_bg2">
       			<h2>Rates</h2>
                <div class="table-responsive">
             <table class="table table-bordered">
                    <thead>
                      <tr>
                      	<th width="200px">Season Period</th>
                        <th width="70px">Weekly</th>
                        <th width="95px">Week night</th>
                        <th width="110px">Weekend night</th>
                        <th width="110px">Minimum Stay</th>
                        <th width="150px">Changeover Day </th>
                                           </tr><tr>     	
                        <td>My Standard Rate <span>2016-03-01 - 2017-03-31</span></td>
                        <td>€&nbsp;795						 </td>
                        <td>€&nbsp;164						</td>
                        <td>€&nbsp;164						</td>
                        <td>3 nights</td>
                        <td>Any day</td>              
                    </tr>
                                            
                  </thead></table>
                  </div>
                 <!-- <h3></h3>
                  <p></p>
                  <p>&euro;200</p>
                  <p> &euro;0</p>-->
            </div>
        </div>
      </div>
    <div role="tabpanel" class="tab-pane active" id="Videos">
    	<div class="row">
        	
        </div>
    </div>
    
    <div role="tabpanel" class="tab-pane active" id="Reviews">
    	<div class="row">
        	<div class="white_bg white_bg1 white_bg2">
       			<h2>Reviews</h2>
                <div class="comments">

        <h3>Be the first to write a         Reviews</h3>
    
                  
        <div class="clearfix"></div>
      </div>
      <form id="formReview" action="" method="POST">
      <h2>Add Your Reviews</h2>
             <div class="row">
             	
             <div class="col-lg-4 col-md-4  col-sm-4  col-xs-12 form-group">
             <label>When did you stay at this property?:</label>
             	<select class="form-control" id="monthReview" name="monthReview" required>
                	<option value="-Month-">-Month-</option>
                    <option value="January">January</option>
                    <option value="February">February</option>
                    <option value="March">March</option>
                    <option value="April">April</option>
                    <option value="May">May</option> 
                    <option value="June">June</option>
                    <option value="July">July</option>
                    <option value="August">August</option>
                    <option value="September">September</option>
                    <option value="October">October</option>
                    <option value="November">November</option>
                    <option value="December">December</option>
				</select>
             
             </div>
            <div class="col-lg-4 col-md-4 col-sm-4   col-xs-12 form-group">
            <label>&nbsp;</label>
             		<select class="form-control" id="yearReview" name="yearReview" required>
                    	<option value="">-Year-</option>
                        <option value="2014">2014</option>
                        <option value="2015">2015</option>
                                            </select>
              		
             </div>
             <div class="col-lg-4 col-md-4 col-sm-4   col-xs-12 form-group">
            <label>Please enter a title for your review:</label>
             		<input name="titleReview" type="text" class="form-control" id="titleReview" required>
              		
             </div>
            
              
             </div>
             <div class="row">
             	
             <div class="col-lg-12 form-group">
				 
             <label>Your review:</label>
             	<textarea name="messageReview"  class="form-control" cols="" rows="" placeholder="Required field" id="messageReview" required></textarea>
             
             </div>          
               
             </div>
             <div class="row">
             	
              <div class="col-lg-4 col-md-4 col-sm-4   col-xs-12 form-group">
             <label>Where do you live?:</label>
             	<input name="fromReview" type="text" class="form-control" id="fromReview" required>
             
             </div>
            
             <div class="col-lg-4 col-md-4 col-sm-4   col-xs-12 form-group">
            <label>Name:</label>
             		<input name="nameReview" type="text" class="form-control" placeholder="Required field" id="nameReview" required>
              		
             </div>
             <div class="col-lg-4 col-md-4 col-sm-4   col-xs-12 form-group">
            <label>Email Id:</label>
             		<input name="emailReview" type="text" class="form-control" placeholder="Required field" id="emailReview" required>
              		
             </div>
            
               
             </div>
             <div class="row">
				 
             	
             	
              <div class="col-lg-4 col-md-4 col-sm-4   col-xs-12 form-group">
					<button class="btn" id="reviewSubmit" type="submit" name="reviewSubmit">Submit Review</button>
					
                          </div>
            
            
               
             </div></form>
            </div>
        </div>
    </div>
   
  </div>

</div>

      	
                </div>
                <div class="col-lg-3 col-md-3  col-sm-3  col-xs-12">
                	<div class="white_bg white_bg1 white_bg2">
                    	<h2>Property Details</h2>
                        <table class="table ">
                          <tbody>
                            <tr><td width="50%">Property type</td><td>: Condo</td> </tr>
                            <tr><td>Sleeps </td><td>: 4</td> </tr>
                            <tr><td>Bedrooms</td><td>: 2</td></tr>
                            <tr><td>Bathrooms</td><td>: 2</td></tr>
                            <tr><td>Minimum Stay</td><td>: 3 nights</td></tr>
                          </tbody>
                        </table>
                    </div>
                    <div class="white_bg white_bg1 white_bg2">
                    	<h2>Owner Profile</h2>
                        <div class="owner_profile">
                            <div style=" width:130px;height:130px; margin:auto;text-align:center;margin-bottom:40px">
                            <img src="http://cdn3.rd.io/user/no-user-image-square.jpg" style=" max-width:100%; max-height:100%; margin:10px auto;">			   
</div>	
                          
            	<div class="text-center"><button class="btn " data-toggle="modal" data-target="#myModal">Contact the owner</button></div>
           
            			</div>
                    </div>
                    
                </div>
            </div>
        </div>
        
    </div>
    </div>

  
<?php include 'footer.php';?>
 <script type="text/javascript" src="js/slider.js"></script>
 
