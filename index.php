<?php include 'top-header.php';?>  
<?php include 'header.php';?>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="images/ny.jpg" alt="New York" width="1200" height="700">
        <div class="carousel-caption">
          <h3>Lorem ipsum dolor sit amet</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, andit praesentsed diam nonummy nibh</p>
        </div>      
      </div>

      <div class="item">
        <img src="images/chicago.jpg" alt="Chicago" width="1200" height="700">
        <div class="carousel-caption">
          <h3>Lorem ipsum dolor sit amet</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, andit praesentsed diam nonummy nibh</p>
        </div>      
      </div>
    
      <div class="item">
        <img src="images/la.jpg" alt="Los Angeles" width="1200" height="700">
        <div class="carousel-caption">
          <h3>Lorem ipsum dolor sit amet</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, andit praesentsed diam nonummy nibh</p>
        </div>      
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>
<section class="about-section">
	<div class="container">
    	<div class="col-xs-6 col-xs-offset-3 text-center">
        	<h3>Lorem ipsum dolor sit amet</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, andit praesentsed diam nonummy nibh. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, andit praesentsed diam nonummy .Lorem ipsum dolor sit amet, consectetuer adipiscing elit, andit praesentsed diam nonummy .Lorem ipsum dolor sit amet, consectetuer adipiscing elit, andit praesentsed diam nonummy</p>
        
        </div>
    </div>
		
</section>
<section class="ourvillas">
	<div class="container">
    	<div class="col-xs-6">
        	<div class="traveladeas-box">
            	<a href="villa1.php">
            	<img src="images/la.jpg" width="100%" height="auto">
                <div class="traveladeas-overlay">
                	<div class="box-table">
                    	<div class="table-cell">
                        	<h3>Lorem ipsum dolor</h3>
                        </div>
                    </div>
                </div>
               </a>
        </div>
        </div>
        <div class="col-xs-6">
        	<div class="traveladeas-box">
            	<a href="villa2.php">
            	<img src="images/chicago.jpg" width="100%" height="auto">
                <div class="traveladeas-overlay">
                	<div class="box-table">
                    	<div class="table-cell">
                        	<h3>Lorem ipsum dolor</h3>
                        </div>
                    </div>
                </div>
                </a>
            </div>
        </div> 
     </div>
</section>
  
<?php include 'footer.php';?>

 