<?php include 'top-header.php';?>  
<?php include 'header.php';?>

<div id="googleMap"></div>
<form method="post" action="mailsend.php">
<div id="contact" class="container">
  <h3 class="text-center">Contact</h3>
 

  <div class="row">
    <div class="col-md-4">
    
      <p><span class="glyphicon glyphicon-map-marker"></span>calle del parque 110 cond balmoral apt penthouse 13 b san juan pr 00911</p>
      <p><span class="glyphicon glyphicon-phone"></span>Phone: 787-944-4225</p>
      <p><span class="glyphicon glyphicon-envelope"></span>Email: jblanch4585@yahoo.com</p>	   
    </div>
    <div class="col-md-8">
      <div class="row">
        <div class="col-sm-6 form-group">
          <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
        </div>
        <div class="col-sm-6 form-group">
          <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
        </div>
      </div>
      <textarea class="form-control" id="comments" name="comments" placeholder="Comment" rows="5"></textarea>
      <br>
      <div class="row">
        <div class="col-md-12 form-group">
          <button class="btn pull-right" type="submit">Send</button>
        </div>
      </div>	
    </div>
  </div>
  
</div>

</form>
  
<?php include 'footer.php';?>


<!-- Add Google Maps -->
<script src="js/map"></script>
<script>
var myCenter = new google.maps.LatLng(41.878114, -87.629798);

function initialize() {
var mapProp = {
center:myCenter,
zoom:12,
scrollwheel:false,
draggable:false,
mapTypeId:google.maps.MapTypeId.ROADMAP
};

var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker = new google.maps.Marker({
position:myCenter,
});

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>
 
